# Written by Dardel (dardel.cc)
# See https://git.dardel.cc/Dardel/Splatoon-3-Random-weapon-picker/src/branch/main/LICENSE for licensing

import random

f = open("weapons.txt")
file_contents = f.read()
words = list(map(str, file_contents.split()))
print(random.choice(words))